".vimrc
"Author: Paul McQuade <paulmcquad@gmail.com"

filetype off
execute pathogen#infect()
filetype plugin indent on

" }}}
" Basic options ----------------------------------------------------------- {{{
syntax on se number title hlsearch incsearch
set encoding=utf-8
set tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab
set laststatus=2
set showmode
set showcmd
set ruler
set list
set linebreak
set colorcolumn=+1
set nocompatible " Use Vim settings, rather then Vi settings

" Reduce timeout after <ESC> is recvd. This is only a good idea on fast links.
set ttimeout
set ttimeoutlen=20
set notimeout

" highlight vertical column of cursor
au WinLeave * set nocursorline nocursorcolumn
au WinEnter * set cursorline
set cursorline

:set guioptions+=m
:set guioptions+=l
"http://vim.wikia.com/wiki/Restore_missing_gvim_menu_bar_under_GNOME"

"Load custom settings
source ~/.vim/colors/VimColour.vim
source ~/.vim/startup/mappings.vim
runtime bundles/tplugin_vim/macros/tplugin.vim

" "Uppercase word" mapping.
"
" This mapping allows you to press <c-u> in insert mode to convert the current
" word to uppercase.  It's handy when you're writing names of constants and
" don't want to use Capslock.
"
" To use it you type the name of the constant in lowercase.  While your
" cursor is at the end of the word, press <c-u> to uppercase it, and then
" continue happily on your way:
"
"                            cursor
"                            v
"     max_connections_allowed|
"     <c-u>
"     MAX_CONNECTIONS_ALLOWED|
"                            ^
"                            cursor
"
" It works by exiting out of insert mode, recording the current cursor location
" in the z mark, using gUiw to uppercase inside the current word, moving back to
" the z mark, and entering insert mode again.
"
" Note that this will overwrite the contents of the z mark.  I never use it, but
" if you do you'll probably want to use another mark.
inoremap <C-u> <esc>mzgUiw`za

" Split line (sister to [J]oin lines)
" The normal use of S is covered by cc, so don't worry about shadowing it.
nnoremap S i<cr><esc>^mwgk:silent! s/\v +$//<cr>:noh<cr>`w

"<Leader> = \"
"<c-y>, (Crtl y ,)
"www.bytefluent.com/vivify"
"/usr/share/vim/vim74"
